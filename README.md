<!DOCTYPE html>
<html>
<head>
<h1>Box Builder</h1>
<p>Start Building</p>
<title>test</title>
<style>
	body {
    	text-align: center;
    }
    .common {
    	height: 16px;
    	width: 20px;
    	color: white;
    	font-size: 13px;
    	font-weight: 600;
    	margin-left: 2px;
    	margin-right: 2px;
    }
	.c1 {
    	background-color: blue;
    }
    .c2 {
    	background-color: black;
    }
    .b-top-color {
    	border-top: 4px solid #f78d13;
    }
    .b-top {
    	border-top: 4px solid #ffffff;
    }
    
    .count-box {
    	margin-top: 16px;
    	display: flex;
    	justify-content: center;
    }
    .add-button {
    	outline: 0px;
        width: 60px;
    	background-color: green;
    	border: 1px solid green;
    	height: 30px;
    	color: white;
    	font-weight: 600;
    }
    .remove-button {
    	outline: 0px;
    	margin-left:10px;
    	width: 100px;
    	background-color: red;
    	border: 1px solid red;
    	height: 30px;
    	color: white;
    	font-weight: 600;
    }
    .total-count {
    	color: #303a50;
    	font-size: 62px;
        margin-top:4px;
    }
</style>
<script>
count = 0;
var arr = new Array();
function myFunction() {
	count=count+1;
    if(count%2 === 0) {
    	if(count%5 === 0) {
    		arr[count-1] ="<div class='c1 b-top-color common'>" + count + "</div>";
        } else {
        	arr[count-1] ="<div class='c1 b-top common'>" + count + "</div>";
        }
    } else {
    	if(count%5 === 0) {
        	arr[count-1] ="<div class='c2 b-top-color common'>" + count + "</div>";
        } else {
    		arr[count-1] ="<div class='c2 b-top common'>" + count + "</div>";
        }
    }
    console.log('arr: ', arr);
   	document.getElementById("countBox").innerHTML = arr.join("");
    document.getElementById("counid").innerHTML = count;
}
function myDelElement() {
	count = count -1;
    if(count <1){
    	count = 0;
    	document.getElementById("countBox").innerHTML = "No Boxes";
        document.getElementById("counid").innerHTML = "0";
    } else {
    	arr.pop();
        document.getElementById("countBox").innerHTML = arr.join("");
        document.getElementById("counid").innerHTML = count;
    }
    
    console.log('del arr: ', arr);
}
</script>
</head>

<body>

<div>
<button class="add-button" type="button" onclick="myFunction()">Add</button>
<button class="remove-button" type="button" onclick="myDelElement()">Remove Box</button>
</div>
 	<div  class="count-box" id="countBox">
 	</div>
 <div class="total-count"id="counid">
 </div>

</body>
</html> 
